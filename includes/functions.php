<?php

function displayLatestArticles($jsonFilePath) {
    // Check if the JSON file exists and is readable
    if (!file_exists($jsonFilePath) || !is_readable($jsonFilePath)) {
        echo "Unable to read JSON file.";
        return;
    }

    // Read JSON file
    $json = file_get_contents($jsonFilePath);
    $articles = json_decode($json, true);

    // Check if JSON decoding was successful
    if ($articles === null) {
        echo "Error decoding JSON file.";
        return;
    }

    // Sort articles by publication date in descending order
    usort($articles, function($a, $b) {
        return strtotime($b['publication_date']) - strtotime($a['publication_date']);
    });

    // Display the latest three articles
    foreach (array_slice($articles, 0, 3) as $article) {
?>
        <div class='article'>
            <h2><?= htmlspecialchars($article['title']) ?></h2>
            <p><?= htmlspecialchars($article['summary']) ?></p>
            <small>Published on: <?= htmlspecialchars($article['publication_date']) ?></small>
        </div>
<?php
    }
}
?>
