PHP Content Management Project
This project consists of a PHP application for dynamic content management, as specified in the technical test.

Installing XAMPP
Download and install XAMPP on your system. You can find XAMPP at https://www.apachefriends.org/index.html.
Start Apache through the XAMPP control panel.
Project Setup
Download the project files.
Place the files in the C:\xampp\htdocs\MarketingDeveloper\ folder on your Windows system.
Running the Project
Open your web browser.
Access the project at http://localhost/MarketingDeveloper/.
Commit Instructions
After making the necessary changes to the project, follow the instructions below to commit to the repository:

Add the remote repository:
bash
git remote add origin <your-repository-url>

Switch to the main branch:
bash
git branch -M main

Push the changes to the repository:
bash
git push -u origin main